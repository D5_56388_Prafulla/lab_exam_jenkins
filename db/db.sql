
create table movie (
  movie_id integer primary key auto_increment,
  movie_title VARCHAR(200),
  movie_release_date DATE,
  movie_time TIME,
  director_name VARCHAR(200)
);

insert into movie (movie_title, movie_release_date, movie_time, director_name) values ('Titanic', '2005-01-02', '02:25:00', 'Nolan');
insert into movie (movie_title, movie_release_date, movie_time, director_name) values ('Tarzan', '2004-02-05', '02:13:00', 'Christopher');
insert into movie (movie_title, movie_release_date, movie_time, director_name) values ('Incredibles', '2003-03-06', '02:00:00', 'Shawn');
insert into movie (movie_title, movie_release_date, movie_time, director_name) values ('Megamind', '2002-04-07', '02:21:00', 'Edward');